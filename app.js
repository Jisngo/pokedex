const pokemonCarte = document.querySelector(".card-pokemon");
const pokemonNom = document.querySelector(".pokemon-nom");
const pokemonImage = document.querySelector(".card-image");
const pokemonImageContainer = document.querySelector(".card-image-container");
const pokemonId = document.querySelector(".pokemon-id");
const pokemonType = document.querySelector(".pokemon-type");
const pokemonStats = document.querySelector(".pokemon-stats");
const bodyBG = document.querySelector("body");

const typeColors = {
  Électrik: "#fac100",
  Normal: "#a0a2a0",
  Feu: "#e72324",
  Eau: "#2481ef",
  Glace: "#3dd9ff",
  Roche: "#b0aa82",
  Vol: "#82baef",
  Plante: "#3da224",
  Psy: "#ef3f7a",
  Ténèbres: "#4f3f3d",
  Insecte: "#92a212",
  Poison: "#923fcc",
  Sol: "#92501b",
  Dragon: "#4f60e2",
  Acier: "#60a2b9",
  Combat: "#ff8100",
  Fée: "#ef70ef",
  Spectre: "#703f70",
  defaut: "#68a090"
}

const recherchePokemon = event => {
  event.preventDefault();
  const {value} = event.target.pokemon;
  fetch(`https://tyradex.vercel.app/api/v1/pokemon/${value.toLowerCase()}`)
    .then(data => data.json())
    .then(response => pokemonData(response))

const pokemonData = data => {
  // on recupere les images, les types et les stats de nos pokemon dans des objets
  const sprites = data.sprites.front_default;
  const {stats, types} = data;
  console.log(data);

  // on remplie nos élément avec notre API
  pokemonNom.textContent = data.name.fr;
  pokemonImage.setAttribute('src', data.sprites.regular);
  pokemonId.textContent = `N° ${data.pokedex_id}`;

  // funcCardColor(types);
  funcPokemonTypes(types)
  funcPokemonStats(stats)
}

const funcPokemonTypes = types => {
  pokemonType.innerText = ``;
  types.forEach(type => {
    pokemonType.innerHTML = `<p>TYPE(S) : ${types.map(type => type.name).join(' / ')}</p>`;
    if (types.length === 2) {
      pokemonType.style.background = `linear-gradient(to right, ${typeColors[types[0].name]} 50%, ${typeColors[types[1].name]} 50%)`;
    } else {
      pokemonType.style.background = typeColors[type.name];
    }
  });
}
}

const chargement = document.querySelector('.loader');